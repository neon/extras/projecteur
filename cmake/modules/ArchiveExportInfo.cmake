# Fallback for version generation from pure git archive exports

set(GIT_EXPORT_VERSION_SHORTHASH "272b161")
set(GIT_EXPORT_VERSION_FULLHASH "272b16152cb338830d3c5afdb61e0f90eeddb007")
set(GIT_EXPORT_VERSION_BRANCH "tag: v0.8, master") # needs parsing in cmake...
set(HAS_GIT_EXPORT_INFO 1)
